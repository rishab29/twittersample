package com.rishabkapoor.twittersample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;

/**
 * Created by RISHAB on 18-07-2015.
 */
public class Fullscreen extends ActionBarActivity {
    ImageAdapter adapter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image);


        // Get the message from the intent
        /*Intent intent = getIntent();
        int imageInt = getIntent().getIntExtra("Image Int",R.drawable.sample_7); // oops should be a fallback if an error happens
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(adapter.mThumbIds[ImageInt]);*/

        Intent i = getIntent();

        int position = i.getExtras().getInt("id");

        // Open the Image adapter
        ImageAdapter imageAdapter = new ImageAdapter(this);

        // Locate the ImageView in single_item_view.xml
        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        // Get image and position from ImageAdapter.java and set into ImageView
        imageView.setImageResource(imageAdapter.mThumbIds[position]);
    }
}
