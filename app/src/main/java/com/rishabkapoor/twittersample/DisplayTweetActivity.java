package com.rishabkapoor.twittersample;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Created by RISHAB on 16-07-2015.
 */
public class DisplayTweetActivity extends ActionBarActivity {
ImageAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_message);

        // Get the message from the intent
        Intent intent = getIntent();

        //login of the twitter session
        TwitterSession session = Twitter.getSessionManager().getActiveSession();
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(session);

        // Set the text view as the activity layout
      GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
               // Toast.makeText(DisplayTweetActivity.this, "" + position, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(DisplayTweetActivity.this,Fullscreen.class);
                intent.putExtra("id",position);
                startActivity(intent);
            }
        });
        twitterApiClient.getStatusesService().userTimeline(null, "rishabkapoor", 100, null, null, null, null, null, true, new com.twitter.sdk.android.core.Callback<List<Tweet>>() {

            @Override
            public void success(Result<List<Tweet>> result) {

                //GridView gridview = (GridView) findViewById(R.id.gridview);
                //gridview.setAdapter(new ImageAdapter(DisplayTweetActivity.this, result.data));
                //gridview.setAdapter(new ImageAdapter());


            }

            @Override
            public void failure(TwitterException e) {

            }
        });




    }
}








